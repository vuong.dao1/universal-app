import Module from './module';

/**
 * Entry point of the module
 */
export const modules = new Module();