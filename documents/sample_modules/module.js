export default class Module {
  /**
   * Must be defined from each platforms
   */
  get diffPropPerPlatform() {
    throw new Error('must define');
  }

  /**
   * Shared function
   */
  commonFunc() {
    return true;
  }
}
