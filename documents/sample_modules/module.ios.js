import BaseModule from './module';

export default class Module extends BaseModule {
  get diffPropPerPlatform() {
    return 'android';
  }
}
