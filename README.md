### Web / Desktop app

This project supports building applications for both web & desktop version at same time.

### Install packages

Because of using Yarn workspace, so please read the [Install package](https://github.com/0xgenoskwa/jam-frontend/tree/master/documents/install_package.md) to know the correct way to install new packages.