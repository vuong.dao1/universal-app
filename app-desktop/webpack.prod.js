const path = require('path');
const { merge } = require('webpack-merge');
const { mainProcessCommonConfig, rendererProcessCommonConfig } = require('./webpack.common');

const mainProcessConfig = merge(mainProcessCommonConfig, {
  mode: 'production',
  output: {
    path: path.resolve(__dirname, 'dist', 'main'),
    filename: 'index.js',
  },
  devtool: 'source-map',
});

const rendererProcessConfig = merge(rendererProcessCommonConfig, {
  mode: 'production',
  output: {
    path: path.resolve(__dirname, 'dist', 'renderer'),
    filename: 'renderer.js',
  },
  devtool: 'source-map',
});

module.exports = [mainProcessConfig, rendererProcessConfig];
