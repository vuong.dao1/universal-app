const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { merge } = require('webpack-merge');

const extensions = ['.desktop.js', '.desktop.jsx', '.js', '.jsx'];
const alias = {
  '@shared': path.resolve(__dirname, '../shared'),
};

const sharedConfig = {
  module: {
    rules: [
      {
        test: /.(js|jsx)$/,
        include: [
          path.resolve(__dirname, 'src'),
          path.resolve(__dirname, '..', 'shared'),
        ],
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-react'],
        },
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          // Creates `style` nodes from JS strings
          'style-loader',
          // Translates CSS into CommonJS
          {
            loader: 'css-loader',
            options: {
              modules: true,
            },
          },
          // Compiles Sass to CSS
          'sass-loader',
        ],
      },
    ],
  },
};

const mainProcessCommonConfig = merge(sharedConfig, {
  entry: path.join(__dirname, 'src', 'main', 'index'),
  resolve: {
    extensions,
  },
  target: 'electron-main',
});

const rendererProcessCommonConfig = merge(sharedConfig, {
  entry: path.join(__dirname, 'src', 'renderer', 'index'),
  resolve: {
    extensions,
    alias,
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, 'src', 'renderer', 'index.html'),
    }),
  ],
  target: 'electron-renderer',
});

module.exports = { mainProcessCommonConfig, rendererProcessCommonConfig };
