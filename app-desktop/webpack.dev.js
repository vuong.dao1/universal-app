const path = require('path');
const webpack = require('webpack');
const { merge } = require('webpack-merge');
const { rendererProcessCommonConfig } = require('./webpack.common');
const BASE_CONFIG = require('../config/config.base');

const rendererProcessConfig = merge(rendererProcessCommonConfig, {
  mode: 'development',
  output: {
    path: path.resolve(__dirname, 'dist', 'renderer'),
    filename: 'renderer.js',
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
  ],
  devServer: {
    index: 'index.html',
    inline: true,
    port: BASE_CONFIG.PROJECT_LOCAL_PORT.DESKTOP,
  },
  devtool: 'source-map',
});

module.exports = rendererProcessConfig;
