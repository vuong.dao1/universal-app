# JAM - Desktop

Development guide for JAM desktop (MacOS).

### Setup

Install dependencies
``` sh
yarn install
```

### Development
``` sh
yarn desktop:dev
```

### Build for production
``` sh
yarn desktop:build
```

JAM app will be in `release` folder.