const path = require('path');

const projectDirs = ['', 'app-web', 'app-desktop'].map((target) => path.join(__dirname, target));

const config = {
  extends: ['airbnb', 'airbnb/hooks'],
  env: {
    browser: true,
    node: true,
  },
  settings: {
    'import/resolver': {
      alias: {
        map: [
          ['@shared', path.join(__dirname, 'shared/index')],
          ['@shared/*', path.join(__dirname, 'shared/*')],
        ],
      },
      node: {
        paths: projectDirs.map((target) => path.join(target, 'node_modules')),
      },
    },
  },
  rules: {
    'import/no-extraneous-dependencies': ['error', { packageDir: projectDirs }],
    'react/jsx-filename-extension': ['error', { extensions: ['.js', '.jsx'] }],
  },
};

module.exports = config;
