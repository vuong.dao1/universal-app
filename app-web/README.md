# JAM - Web

Development guide for JAM web.

### Setup

Install dependencies
``` sh
yarn install
```

### Development
``` sh
yarn web:dev
```

### Build for production
``` sh
yarn web:build
```

JAM app will be in `dist` folder.