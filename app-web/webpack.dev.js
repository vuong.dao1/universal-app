const path = require('path');
const webpack = require('webpack');
const { merge } = require('webpack-merge');
const commonConfig = require('./webpack.common');
const BASE_CONFIG = require('../config/config.base');

module.exports = merge(commonConfig, {
  mode: 'development',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].[hash].js',
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
  ],
  devServer: {
    open: true,
    index: 'index.html',
    inline: true,
    port: BASE_CONFIG.PROJECT_LOCAL_PORT.WEB,
  },
  devtool: 'source-map',
});
