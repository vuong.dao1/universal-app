import ReactDOM from 'react-dom';
import React from 'react';
import App from '@shared';

ReactDOM.render(<App />, document.getElementById('root'));
