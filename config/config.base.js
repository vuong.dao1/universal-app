const PROJECT_LOCAL_PORT = {
  WEB: 8080,
  DESKTOP: 8082,
};

module.exports = {
  PROJECT_LOCAL_PORT,
};
